<?php

use App\Http\Controllers\CashFlowController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use App\Models\CashFlow;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::get('/tes', function () {
    return view('tes');
});

Route::get('/tes2', function () {
    return view('/tes2');
});

Route::get('/cashflow', [CashFlowController::class, 'datatables'])->name('cashflow');
Route::get('/edit', function ($id) {
    dd('Edit ' . $id);
})->name('edit');

Route::get('manual-table', function () {
    return view('manual-table');
});

Route::get('cashflow-delete/{id}', function ($id) {
    dd('Delete ' . $id);
});

Route::get('cashflow-edit/{id}', function ($id) {
    dd('Edit ' . $id);
});

Route::get('/datatables-js', function () {
    $cashflows = CashFlow::all();
    return view('datatablesJs', compact(
        'cashflows'
    ));
});

// Route::get('tambah-user', [UserController::class, 'index']);
Route::post('tambah-user', [UserController::class, 'store']);


// ///////////////////////////////////////////////////////////////////////////////////////////////////////////

Route::get('/', [CashFlowController::class, 'create'])->middleware('auth');

Route::get('/analytic', [CashFlowController::class, 'analytic'])->middleware('auth');

Route::get('/opsi/tambah-kategori', [CategoryController::class, 'create'])->name('tambah-kategori');
