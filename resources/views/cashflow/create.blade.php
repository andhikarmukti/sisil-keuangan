@extends('layouts.main')

@section('content')
    <livewire:cashflow-form />
    <script>
        var nominal = document.getElementById('nominal');
        nominal.addEventListener('keyup', function(e) {
            console.log(nominal);
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            nominal.value = formatRupiah(this.value, '');
        });

        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                nominal = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                nominal += separator + ribuan.join('.');
            }

            nominal = split[1] != undefined ? nominal + ',' + split[1] : nominal;
            return prefix == undefined ? nominal : (nominal ? '' + nominal : '');
        }
    </script>
    <script>
        $('body').click(function() {
            $('body .button-close-flash').click(function() {
                console.log('ok');
                $('.flash-message').toggle("hidden");
            });
        });
    </script>
@endsection
