@extends('layouts.datatable')

@section('datatable')
<a href="/" class="btn btn-primary">kembali</a>

<div class="container">
    <div class="page-header header">
        <h2>Table Cashflow</h2>
    </div>
    <div class="row">
        <div class="col">
            <table id="cashflow-datatable" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Jenis</th>
                        <th>Entitas</th>
                        <th>Kategori</th>
                        <th>Currency</th>
                        <th>Nominal</th>
                        <th>Payment</th>
                        <th>Keterangan</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="">
                    @foreach ($cashflows as $cashflow)
                    <tr>
                        <td>{{ date_format(date_create($cashflow->tanggal), 'd/m/Y') }}</td>
                        <td>{{ $cashflow->jenis }}</td>
                        <td>{{ $cashflow->entitas }}</td>
                        <td>{{ $cashflow->kategori }}</td>
                        <td>{{ $cashflow->currency }}</td>
                        <td>{{ $cashflow->currency == 'Rupiah' ? 'Rp ' : '$ ' }}
                            {{ number_format($cashflow->nominal, 0, ',', '.') }}</td>
                        <td>{{ $cashflow->payment }}</td>
                        <td>{{ $cashflow->keterangan }}</td>
                        <td>
                            <a href="edit/{{ $cashflow->id }}" class="badge bg-warning">edit</a>
                            <a href="" class="badge bg-danger">delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr id="sort">
                        <th></th>
                        <th>Jenis</th>
                        <th>Entitas</th>
                        <th>Kategori</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Keterangan</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="">
        <img class="center-block" src="/img/{{ $gambar }}.png" alt="">
    </div>
</div>
<!--Jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!--Boostrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!--DataTables -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<!--DateRangePicker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript">
    //fungsi untuk filtering data berdasarkan tanggal
        var start_date;
        var end_date;
        var DateFilterFunction = (function(oSettings, aData, iDataIndex) {
            console.log(dateStart);
            var dateStart = parseDateValue(start_date);
            var dateEnd = parseDateValue(end_date);
            //Kolom tanggal yang akan kita gunakan berada dalam urutan 2, karena dihitung mulai dari 0
            //nama depan = 0
            //nama belakang = 1
            //tanggal terdaftar =2
            var evalDate = parseDateValue(aData[0]);
            if ((isNaN(dateStart) && isNaN(dateEnd)) ||
                (isNaN(dateStart) && evalDate <= dateEnd) ||
                (dateStart <= evalDate && isNaN(dateEnd)) ||
                (dateStart <= evalDate && evalDate <= dateEnd)) {
                return true;
            }
            return false;
        });

        // fungsi untuk converting format tanggal dd/mm/yyyy menjadi format tanggal javascript menggunakan zona aktubrowser
        function parseDateValue(rawDate) {
            var dateArray = rawDate.split("/");
            var parsedDate = new Date(dateArray[2], parseInt(dateArray[1]) - 1, dateArray[
                0]); // -1 because months are from 0 to 11
            return parsedDate;
        }

        $(document).ready(function() {
            //konfigurasi DataTable pada tabel dengan id cashflow-datatable dan menambahkan  div class dateseacrhbox dengan dom untuk meletakkan inputan daterangepicker
            $('#cashflow-datatable #sort th').each(function() {
                var keyword = $(this).text();
                if (keyword != '') {
                    var title = $(this).text();
                    $(this).html('<input type="text" style="width:100%" placeholder="Search"/>');
                }
            });

            var $dTable = $('#cashflow-datatable').DataTable({
                initComplete: function() {
                    // Apply the search
                    this.api().columns().every(function() {
                        var that = this;

                        $('input', this.footer()).on('keyup change clear', function() {
                            console.log(this.value);
                            if (that.search() !== this.value) {
                                that
                                    .search(this.value)
                                    .draw();
                            }
                        });
                    });
                },

                dom: "<'row'<'col-sm-4'l><'col-sm-5' <'datesearchbox'>><'col-sm-3'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",

                footerCallback: function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,.Rp]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(5)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(5, {
                            page: 'current'
                        })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);


                    // Total filtered rows on the selected column (code part added)
                    var sumCol5Filtered = display.map(el => data[el][5]).reduce((a, b) =>
                        intVal(a) +
                        intVal(b), 0);

                    // Update footer
                    $(api.column(5).footer()).html(
                        new Intl.NumberFormat("id-ID", {
                            style: "currency",
                            currency: "IDR",
                            minimumFractionDigits: 0
                        }).format(sumCol5Filtered)
                    );
                }
            });

            //menambahkan daterangepicker di dalam datatables
            $("div.datesearchbox").html(
                '<div class="input-group"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range.."> </div>'
            );

            document.getElementsByClassName("datesearchbox")[0].style.textAlign = "left";

            //konfigurasi daterangepicker pada input dengan id datesearch
            $('#datesearch').daterangepicker({
                autoUpdateInput: false
            });

            //menangani proses saat apply date range
            $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format(
                    'DD/MM/YYYY'));
                start_date = picker.startDate.format('DD/MM/YYYY');
                end_date = picker.endDate.format('DD/MM/YYYY');
                $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
                $dTable.draw();
            });

            $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                start_date = '';
                end_date = '';
                $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(
                    DateFilterFunction, 1));
                $dTable.draw();
            });
        });
</script>

@endsection
