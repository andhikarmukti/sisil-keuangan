<!DOCTYPE html>
<html lang="en">

<head>
    <title>Datatables JS</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!--DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" />
    <!--Daterangepicker -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style type="text/css">
        .header h2 {
            font-weight: lighter;
            text-align: center;
            margin: 0
        }

        .header h3 {
            font-weight: lighter;
            text-align: center;
            margin: 0
        }

        .number {
            text-align: right;
        }
    </style>
</head>

<body>
    @yield('datatable')
</body>

</html>
