<div>
    <h2 class="text-center text-5xl font-bold mt-10 text-white">Add New Category</h2>

    @if (session('store'))
    <div class="flash-message text-center py-4 lg:px-4">
        <div class="p-2 bg-green-300 items-center text-black leading-none lg:rounded-full flex lg:inline-flex"
            role="alert">
            <span class="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">Mantap</span>
            <span class="font-semibold mr-2 text-left flex-auto">{{ session('store') }}</span>
            <button class="button-close-flash font-bold text-blue-700">X</button>
        </div>
    </div>
    @endif

    <form wire:submit.prevent="store" method="POST" class="mx-auto mt-5 border-2 rounded-lg w-1/4">
        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 bg-white sm:p-6">
                <div class="grid grid-cols-6 gap-6">
                    <div class="col-span-6">
                        <label for="entitas" class="block text-sm font-medium text-gray-700">Entitas</label>
                        <select wire:model="entitas" name="entitas" id="entitas"
                            class="@error('entitas')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            <option value="" class="text-gray-400">Pilih Entitas</option>
                            @foreach ($entitasOptions as $entitas)
                            <option value="{{ $entitas->entitas }}">{{ $entitas->entitas }}</option>
                            @endforeach
                        </select>
                        @error('entitas')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-span-6 sm:col-span-6 lg:col-span-6">
                        <label for="kategori" class="block text-sm font-medium text-gray-700">Category Name</label>
                        <input wire:model="kategori" type="text" name="kategori" id="kategori"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    </div>
                </div>
            </div>
            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                <button type="submit"
                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Add
                </button>
            </div>
        </div>
    </form>
    {{-- <img class="mx-auto"
        src="https://stickershop.line-scdn.net/stickershop/v1/product/7506797/LINEStorePC/main.png;compress=true"
        alt=""> --}}
</div>
