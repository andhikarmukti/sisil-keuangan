<div>
    <h2 class="text-center text-5xl font-bold mt-10 text-white">Input Cashflow</h2>

    @if (session('store'))
    <div class="flash-message text-center py-4 lg:px-4">
        <div class="p-2 bg-green-300 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex"
            role="alert">
            <span class="flex rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">Mantap</span>
            <span class="font-semibold mr-2 text-left flex-auto">{{ session('store') }}</span>
            <button class="button-close-flash font-bold text-blue-700">X</button>
        </div>
    </div>
    @endif


    <form wire:submit.prevent="store" method="POST"
        class="{{ $jenisSelect == false ? 'w-1/4' : 'w-1/2' }} mx-auto mt-5 border-2 rounded-lg">
        <div class="shadow overflow-hidden sm:rounded-md">
            <div class="px-4 py-5 bg-white sm:p-6">
                <div class="grid grid-cols-6 gap-6">
                    <div class="col-span-6 {{ $jenisSelect == false ? 'sm:col-span-6' : 'sm:col-span-3' }}">
                        <label for="tanggal"
                            class="block text-sm font-medium text-gray-700 {{ $jenisSelect == false ? 'text-center' : '' }}">Tanggal</label>
                        <input wire:model='tanggal' type="date" name="tanggal" id="tanggal"
                            class=" @error('tanggal')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('tanggal')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>

                    @if ($jenisSelect == true)
                    <div class="col-span-6 sm:col-span-3">
                        <label for="jenis" class="block text-sm font-medium text-gray-700">Jenis</label>
                        <select wire:model="jenis" name="jenis" id="jenis"
                            class="@error('jenis')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            <option value="" class="text-gray-400">Pilih jenis</option>
                            <option value="Pemasukan">Pemasukan</option>
                            <option value="Pengeluaran">Pengeluaran</option>
                        </select>
                        @error('jenis')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>
                    @endif

                    @if ($entitasSelect == true)
                    <div class="col-span-6 sm:col-span-3">
                        <label for="email-address" class="block text-sm font-medium text-gray-700">Entitas</label>
                        <select wire:model="entitas" name="entitas" id="entitas"
                            class="@error('entitas')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            <option value="" class="text-gray-400">Pilih Entitas</option>
                            @foreach ($entitasOptions as $e)
                            <option value="{{ $e->entitas }}">{{ $e->entitas }}</option>
                            @endforeach
                        </select>
                        @error('entitas')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>
                    @endif

                    @if ($kategoriSelect == true)
                    <div class="col-span-6 sm:col-span-3">
                        <label for="kategori" class="block text-sm font-medium text-gray-700">Kategori</label>
                        <select wire:model="kategori" id="kategori" name="kategori"
                            class="@error('kategori')
                            border-red-600
                            @enderror mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            @foreach ($kategoriOptions as $k)
                            <option value="{{ $k->kategori }}">{{ $k->kategori }}</option>
                            @endforeach
                        </select>
                        @error('kategori')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>
                    @endif

                    @if ($currencySelect == true)
                    <div class="col-span-3">
                        <label for="street-address" class="block text-sm font-medium text-gray-700">Currency</label>
                        <select wire:model="currency" name="currency" id="currency"
                            class="@error('currency')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            <option value="">Pilih Currency</option>
                            <option value="Rupiah">Rupiah</option>
                            <option value="Dollar">Dollar</option>
                        </select>
                        @error('currency')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>
                    @endif

                    <div {{ $nominalSelect==true ? '' : 'hidden' }} class="col-span-6 sm:col-span-3 lg:col-span-3">
                        <label for="nominal" class="block text-sm font-medium text-gray-700"
                            type-currency="IDR">Nominal</label>
                        <input wire:model="nominal" type="text" name="nominal" id="nominal"
                            class="@error('nominal')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                        @error('nominal')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>

                    @if ($paymentSelect == true)
                    <div class="col-span-6 sm:col-span-3 lg:col-span-3">
                        <label for="payment" class="block text-sm font-medium text-gray-700">Payment</label>
                        <select wire:model="payment" name="payment" id="payment"
                            class="@error('payment')
                            border-red-600
                            @enderror mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            <option value="">Pilih Payment</option>
                            @foreach ($paymentOptions as $paym)
                            <option value="{{ $paym->payment }}">{{ $paym->payment }}</option>
                            @endforeach
                        </select>
                        @error('payment')
                        <small class="text-red-600">{{ $message }}</small>
                        @enderror
                    </div>
                    @endif

                    @if ($keteranganSelect == true)
                    <div class="col-span-6 sm:col-span-3 lg:col-span-3">
                        <label for="keterangan" class="block text-sm font-medium text-gray-700">Keterangan</label>
                        <input wire:model="keterangan" type="text" name="keterangan" id="keterangan"
                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                    </div>
                    @endif

                </div>
            </div>
            <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                @if ($saveSelect == true)
                <button type="submit"
                    class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Save
                </button>
                @endif
            </div>
        </div>
    </form>
</div>
