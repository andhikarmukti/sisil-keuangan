<div>
    <div class="flex flex-col p-16">
        <h1 class="mb-16 text-center text-5xl">Table Cashflow</h1>
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full">
                        <input wire:model='search' type="text" class="rounded-3xl mb-5 w-96" placeholder="Search.."
                            autofocus>
                        <select wire:model='entitas_selected' class="ml-5 rounded-3xl mb-5 w-96">
                            <option value="">Pilih Entitas</option>
                            @foreach ($entitas as $e)
                            <option value="{{ $e->entitas }}">{{ $e->entitas }}</option>
                            @endforeach
                        </select>
                        <select class="ml-5 rounded-3xl mb-5 w-96">
                            <option value="">Pilih Kategori</option>
                            @foreach ($categories as $category)
                            <option value="">{{ $category->kategori }}</option>
                            @endforeach
                        </select>
                        <thead class="bg-gray-200 divide-gray-200">
                            <tr class="text-center">
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Tanggal
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Jenis
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Entitas
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Kategori
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Currency
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Nominal
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Payment
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Keterangan
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Update By
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @foreach ($cashflows as $cashflow)
                            <tr class="text-center">
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $cashflow->tanggal }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $cashflow->jenis }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $cashflow->entitas }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    {{ $cashflow->kategori }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                    {{ $cashflow->currency }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                    {{ $cashflow->nominal }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                    {{ $cashflow->payment }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                    {{ $cashflow->keterangan }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                    {{ $cashflow->update_by }}
                                </td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                    {{ $cashflows->links() }}

                </div>
            </div>
        </div>
    </div>
</div>
