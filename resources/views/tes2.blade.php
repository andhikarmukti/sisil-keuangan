@extends('layouts.main')

@section('content')
    <div class="py-12">
        <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-blue-200 border-b border-gray-200">
                    <livewire:cash-flow-table2 />
                </div>
            </div>
        </div>
    </div>
@endsection
