@extends('layouts.main')

@section('content')
    <livewire:category-create-form />
    {{-- <livewire:category-create-table /> --}}
    <script>
        $('body').click(function() {
            $('body .button-close-flash').click(function() {
                console.log('ok');
                $('.flash-message').toggle("hidden");
            });
        });
    </script>
@endsection
