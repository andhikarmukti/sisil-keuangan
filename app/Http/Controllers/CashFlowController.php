<?php

namespace App\Http\Controllers;

use App\Models\CashFlow;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Livewire\CashflowForm;
use App\Http\Requests\StoreCashFlowRequest;
use App\Http\Requests\UpdateCashFlowRequest;
use Illuminate\Support\Arr;

class CashFlowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Cashflow';

        return view('cashflow.create', compact(
            'title'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCashFlowRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCashFlowRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function show(CashFlow $cashFlow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function edit(CashFlow $cashFlow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCashFlowRequest  $request
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCashFlowRequest $request, CashFlow $cashFlow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CashFlow  $cashFlow
     * @return \Illuminate\Http\Response
     */
    public function destroy(CashFlow $cashFlow)
    {
        //
    }

    public function datatables(Request $request)
    {
        if ($request->ajax()) {
            $data = CashFlow::all();
            return Datatables::of($data)
                ->addColumn('action', function (CashFlow $cashFlow) {

                    $btn = '<a href="' . route('edit', $cashFlow->id) . '" class="edit badge bg-warning btn-sm">edit</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('tesyajra');
    }

    public function analytic()
    {
        $cashflows = CashFlow::all();
        $title = 'Analytic';
        $gambar = Arr::random([
            'atta',
            'bts',
            'bts2',
            'tukul',
            'kiwil',
            'bts3',
            'dita',
            'sisil',
            'dhilajm',
            'dhilajm2'
        ]);

        return view('cashflow.analytic', compact(
            'cashflows',
            'title',
            'gambar'
        ));
    }
}
