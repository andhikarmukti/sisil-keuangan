<?php

namespace App\Http\Controllers;

use App\Models\Entitas;
use App\Http\Requests\StoreEntitasRequest;
use App\Http\Requests\UpdateEntitasRequest;

class EntitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEntitasRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEntitasRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function show(Entitas $entitas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function edit(Entitas $entitas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEntitasRequest  $request
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEntitasRequest $request, Entitas $entitas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entitas  $entitas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entitas $entitas)
    {
        //
    }
}
