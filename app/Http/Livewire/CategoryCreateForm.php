<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Entitas;
use Livewire\Component;

class CategoryCreateForm extends Component
{
    public $entitas, $kategori;

    public function render()
    {
        return view('livewire.category-create-form', [
            'entitasOptions' => Entitas::all()
        ]);
    }

    public function store()
    {
        Category::create([
            'entitas' => $this->entitas,
            'kategori' => $this->kategori
        ]);

        $this->resetInput();
        session()->flash('store', 'Berhasil menambahkan kategori baru');
    }

    public function resetInput()
    {
        $this->entitas = null;
        $this->kategori = null;
    }
}
