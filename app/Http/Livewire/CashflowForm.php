<?php

namespace App\Http\Livewire;

use App\Models\CashFlow;
use App\Models\Category;
use App\Models\Entitas;
use App\Models\Payment;
use Livewire\Component;

class CashflowForm extends Component
{
    public $tanggal, $jenis, $kategori, $entitas, $currency, $nominal, $payment, $keterangan, $kategoriSelect, $jenisSelect, $entitasSelect, $currencySelect, $nominalSelect, $paymentSelect, $keteranganSelect, $saveSelect;

    public function render()
    {
        if($this->entitas != null){
            $this->kategoriSelect = true;
        }
        if($this->tanggal != null){
            $this->jenisSelect = true;
        }
        if($this->jenis != null){
            $this->entitasSelect = true;
        }
        if($this->kategori != null){
            $this->currencySelect = true;
        }
        if($this->currency != null){
            $this->nominalSelect = true;
        }
        if($this->nominal != null){
            $this->paymentSelect = true;
            $this->saveSelect = true;
        }
        if($this->payment != null){
            $this->keteranganSelect = true;
        }

        return view('livewire.cashflow-form',[
            'kategoriOptions' => Category::whereEntitas($this->entitas)->get(),
            'entitasOptions' => Entitas::all(),
            'paymentOptions' => Payment::all()
        ]);
    }

    protected $rules = [
        'tanggal' => 'required',
        'jenis' => 'required',
        'kategori' => 'required',
        'entitas' => 'required',
        'currency' => 'required',
        'nominal' => 'required',
        'payment' => 'required'
    ];

    public function store()
    {
        $this->validate();

        CashFlow::create([
            'tanggal' => $this->tanggal,
            'jenis' => $this->jenis,
            'kategori' => $this->kategori,
            'entitas' => $this->entitas,
            'currency' => $this->currency,
            'nominal' => str_replace('.', '', $this->nominal),
            'payment' => $this->payment,
            'keterangan' => $this->keterangan,
            'update_by' => auth()->user()->name
        ]);

        $this->resetInput();

        session()->flash('store', 'Berhasil menambahkan data');
    }

    public function resetInput()
    {
        $this->tanggal = null;
        $this->jenis = null;
        $this->kategori = null;
        $this->entitas = null;
        $this->currency = null;
        $this->nominal = null;
        $this->payment = null;
        $this->keterangan = null;

        $this->tanggalSelect = false;
        $this->jenisSelect = false;
        $this->kategoriSelect = false;
        $this->entitasSelect = false;
        $this->currencySelect = false;
        $this->nominalSelect = false;
        $this->paymentSelect = false;
        $this->keteranganSelect = false;
        $this->saveSelect = false;
    }
}
