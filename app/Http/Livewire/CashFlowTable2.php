<?php

namespace App\Http\Livewire;

use App\Models\CashFlow;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\LabelColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\BooleanColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class CashFlowTable2 extends LivewireDatatable
{
    public $model = CashFlow::class;

    public function columns()
    {
        return [
            DateColumn::name('tanggal')
                ->label('Tanggal')
                ->alignCenter()
                ->filterable(),

            Column::name('jenis')
                ->label('Jenis')
                ->alignCenter()
                ->filterable(),

            Column::name('entitas')
                ->label('Entitas')
                ->searchable()
                ->alignCenter()
                ->filterable(),

            Column::name('kategori')
                ->label('Kategori')
                ->alignCenter()
                ->searchable()
                ->filterable(),

            Column::name('currency')
                ->label('Currency')
                ->alignCenter()
                ->searchable(),

            NumberColumn::callback('nominal', function ($nominal) {
                return 'Rp ' . number_format($nominal, 0, ',', '.');
            })
                ->label('Nominal')
                ->alignCenter(),

            Column::name('payment')
                ->label('Payment')
                ->searchable(),

            Column::name('keterangan')
                ->label('Keterangan')
                ->editable()
                ->searchable(),

            Column::action()->label('Action')->alignCenter(),

            // Column::callback('id', function ($id) {
            //     return "
            //     <div>
            //     <a class='text-blue-600' href='" . 'cashflow-edit/' . $id . "'>Edit</a>
            //     <a class='text-red-600 ml-2' href='" . 'cashflow-delete/' . $id . "'>Delete</a>
            //     </div>
            //     ";
            // })
            //     ->label('Action')
        ];
    }
}
