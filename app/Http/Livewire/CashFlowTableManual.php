<?php

namespace App\Http\Livewire;

use App\Models\Entitas;
use Livewire\Component;
use App\Models\CashFlow;
use App\Models\Category;
use Livewire\WithPagination;

class CashFlowTableManual extends Component
{
    use WithPagination;

    public $categories,
        $entitas,
        $entitas_selected,
        $search;

    public function mount()
    {
        $this->categories = Category::all();
        $this->entitas = Entitas::all();
    }

    public function render()
    {
        $cashflows = new CashFlow();

        if ($this->entitas_selected) {
            $cashflows->where('entitas', $this->entitas_selected);
        }

        if ($this->search) {
            $cashflows->where('jenis', 'like', '%' . $this->search . '%')
                ->orWhere('keterangan', 'like', '%' . $this->search . '%')
                ->orWhere('entitas', 'like', '%' . $this->search . '%')
                ->orWhere('kategori', 'like', '%' . $this->search . '%');
        }

        return view('livewire.cash-flow-table-manual', [
            'cashflows' => $cashflows->paginate(10)
        ]);
    }
}
