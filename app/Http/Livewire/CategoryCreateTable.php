<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;

class CategoryCreateTable extends Component
{
    public function render()
    {
        return view('livewire.category-create-table',[
            'categories' => Category::paginate(10)
        ]);
    }
}
